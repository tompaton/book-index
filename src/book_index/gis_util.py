from dataclasses import dataclass
from math import atan, degrees, exp, log, pi, radians, tan


@dataclass
class Coord:
    lat: float
    lon: float

    def __repr__(self) -> str:
        return f"({self.lat}, {self.lon})"


@dataclass(frozen=True)
class Pix:
    x: int
    y: int

    def __str__(self) -> str:
        return str(self.astuple())

    def astuple(self) -> tuple[int, int]:
        return self.x, self.y


def parse_bounds(bounds: str) -> tuple[Coord, Coord, str]:
    top_left_, bottom_right_ = bounds.split(" - ")
    top_, left_ = top_left_[1:-1].split(", ")
    bottom_, right_note = bottom_right_[1:].split(", ")
    right_, note = right_note.split(")", maxsplit=1)
    top = float(top_)
    left = float(left_)
    bottom = float(bottom_)
    right = float(right_)
    return (Coord(top, left), Coord(bottom, right), note)


def bounding_box(
    center_lat: float,
    center_lon: float,
    zoom: float,
    width: int,
    height: int,
    scale: float = 2,
) -> tuple[Coord, Coord]:
    """Get bounding box (top_left, bottom_right) of web mercator image
    centered on (center_lat, center_lon) at zoom level with dimensions
    (width, height).
    """
    # https://gis.stackexchange.com/a/279234

    # zoom factor
    zoom_f = (128 / pi) * pow(2, zoom)

    def coord_to_pix(lat: float, lon: float) -> Pix:
        # convert (lat, lon) degrees to Pix(x, y)
        return Pix(
            int(zoom_f * (radians(lon) + pi)),
            int(zoom_f * (pi - log(tan(pi / 4 + radians(lat) / 2)))),
        )

    def pix_to_coord(pix_x: float, pix_y: float) -> Coord:
        # convert Pix (x, y) to Coord(lat, lon) degrees
        return Coord(
            degrees(2 * atan(exp(pi - pix_y / zoom_f)) - pi / 2),
            degrees(pix_x / zoom_f - pi),
        )

    # middle pixel coordinate
    mid = coord_to_pix(center_lat, center_lon)

    # image pixel boundaries
    return (
        pix_to_coord(mid.x - (width / scale), mid.y - (height / scale)),
        pix_to_coord(mid.x + (width / scale), mid.y + (height / scale)),
    )
