import hashlib
from collections import defaultdict
from collections.abc import Callable, Iterable
from dataclasses import dataclass
from fractions import Fraction
from itertools import combinations, groupby
from math import ceil
from operator import attrgetter
from pathlib import Path
from statistics import median
from typing import Any

import requests
from h3 import h3
from label_layout import LabelLayout, Leader, Port, collapse_items, intersects
from page_boxes import ImageBox
from page_layout import Photo, _Caption
from pdf_canvas import Align, Paragraph, Table, TableDataType, cm

from book_index.gis_util import Coord, Pix

PageNum = tuple[str, str]

Frac = tuple[float, float]  # lon/x, lat/y
Points = list[Frac]


@dataclass
class BookCalendar:
    year: int
    month: int
    month_name: str
    days: list[tuple[int, str]]


@dataclass
class MapImage:
    name: str
    filename: str
    top_left: Coord
    bottom_right: Coord
    width: int
    height: int
    h3resolution: int = 10

    @property
    def dlat(self) -> float:
        return (self.bottom_right.lat - self.top_left.lat) / self.height

    @property
    def dlon(self) -> float:
        return (self.bottom_right.lon - self.top_left.lon) / self.width

    def pix_to_coord(self, x: int, y: int) -> Coord:
        lat = self.top_left.lat + y * self.dlat
        lon = self.top_left.lon + x * self.dlon
        return Coord(lat, lon)

    @property
    def dx(self) -> float:
        return 1.0 / (self.bottom_right.lon - self.top_left.lon)

    @property
    def dy(self) -> float:
        return 1.0 / (self.bottom_right.lat - self.top_left.lat)

    def coord_to_frac(self, lat: float, lon: float) -> Frac:
        return (
            (lon - self.top_left.lon) * self.dx,
            (lat - self.top_left.lat) * self.dy,
        )

    def coord_to_pix(self, lat: float, lon: float) -> Pix:
        fx, fy = self.coord_to_frac(lat, lon)
        return Pix(int(fx * self.width), int(fy * self.height))

    def coord_on_map(self, lat: float, lon: float) -> bool:
        p = self.coord_to_pix(lat, lon)
        return self.pix_on_map(p.x, p.y)

    def coords_on_map(self, *coords: Coord) -> bool:
        return all(self.coord_on_map(coord.lat, coord.lon) for coord in coords)

    def pix_on_map(self, x: int, y: int) -> bool:
        return 0 <= x < self.width and 0 <= y < self.height

    def h3_points(self, h3index: str) -> Points | None:
        points = [self.coord_to_frac(point_lat, point_lon) for point_lat, point_lon in h3.h3_to_geo_boundary(h3index)]
        if any(0 <= x <= 1.0 and 0 <= y <= 1.0 for x, y in points) or self._intersects(points):
            return points

        return None

    def _intersects(self, points: Points) -> bool:
        edges = [
            ((0.0, 0.0), (1.0, 0.0)),
            ((1.0, 0.0), (1.0, 1.0)),
            ((1.0, 1.0), (0.0, 1.0)),
            ((0.0, 1.0), (0.0, 0.0)),
        ]

        for vertex_a, vertex_b in combinations(points, 2):
            for edge_start, edge_end in edges:
                if intersects(vertex_a, vertex_b, edge_start, edge_end):
                    return True
        return False

    def h3index_to_pix(self, h3index: str) -> Pix:
        hex_lat, hex_lon = h3.h3_to_geo(h3index)
        return self.coord_to_pix(hex_lat, hex_lon)

    def __gt__(self, other: "MapImage") -> bool:
        return self.h3resolution < other.h3resolution

    def coord_rect_to_pix(self, top_left: Coord, bottom_right: Coord) -> ImageBox:
        left, top = self.coord_to_pix(top_left.lat, top_left.lon).astuple()
        right, bottom = self.coord_to_pix(bottom_right.lat, bottom_right.lon).astuple()
        return ImageBox(left=left, bottom=bottom, width=right - left, height=bottom - top)


@dataclass
class Label:
    para: Paragraph
    para_text: str
    align: Align
    width: Fraction
    height: Fraction
    radius: Fraction
    markers: list["Marker"]

    _height: Fraction = Fraction("0.5")  # cm

    @classmethod
    def make_label2(
        cls,
        label: str,
        label2: str,
        align: Align,
        scale: Fraction,
        label_width: Fraction,
        markers: list["Marker"],
        marker_radius: Fraction = Fraction("0.1"),
    ) -> "Label":
        para = cls.make_label(label, align, scale, label_width, markers, marker_radius)
        para2 = cls.make_label(label2, align, scale, label_width, markers, marker_radius)
        if round(para.height, 3) == round(para2.height, 3):
            # prefer version without the colon if it is the same size
            # i.e the colon is redundant
            return para2

        return para

    @classmethod
    def make_label(
        cls,
        label: str,
        align: Align,
        scale: Fraction,
        label_width: Fraction,
        markers: list["Marker"],
        marker_radius: Fraction = Fraction("0.1"),
    ) -> "Label":
        para = Paragraph(
            label,
            alignment=align,
            fontName="Vera",
            fontSize=8 / scale,
            leading=10 / scale,
            textColor="black",
        )

        width, height = para.wrap(label_width / scale, cls._height / scale)
        radius = marker_radius / scale

        return cls(para, label, align, width, height, radius, markers)


@dataclass
class Marker:
    page_num: str
    caption: str
    lat: float
    lon: float
    location: str
    h3index: str
    x: int
    y: int

    @property
    def page_num_key(self) -> int:
        try:
            return int(self.page_num)
        except ValueError:
            return {
                "cover": -1,
                "inside front": 0,
                "inside back": 1000,
                "back": 1001,
            }.get(self.page_num, 0)


class MapIndex:
    def __init__(self, map_image: MapImage) -> None:
        self.image: MapImage = map_image
        self.markers: dict[Pix, list[Marker]] = defaultdict(list)
        self.page_points: dict[str, set[Pix]] = defaultdict(set)
        self.top = False
        self.insets: list[str] = []

    def __len__(self) -> int:
        return sum(len(vals) for vals in self.markers.values())

    def __gt__(self, other: "MapIndex") -> bool:
        return self.image > other.image

    def add_photo(
        self,
        lat: float | None,
        lon: float | None,
        h3index: str | None,
        page_num: str,
        slot: str,
        location: str,
        location_h3index: str | None,
    ) -> bool:
        if h3index is not None:
            lat, lon = h3.h3_to_geo(h3index)

        # convert photo lat, lon to mid point of h3 hex at map images resolution
        h3index = h3.geo_to_h3(lat, lon, self.image.h3resolution)
        hex_lat, hex_lon = h3.h3_to_geo(h3index)
        p = self.image.coord_to_pix(hex_lat, hex_lon)
        if self.image.pix_on_map(p.x, p.y):
            if lat is None or lon is None or h3index is None:
                msg = f"{lat=} {lon=} {h3index=}"
                raise ValueError(msg)

            self.markers[p].append(Marker(page_num, slot, lat, lon, location, h3index, p.x, p.y))

            # if not collecting points in a location then these end up in
            # separate points and the marker captions aren't merged properly...
            # so always record the labels at the "location" h3index, not the
            # actual photo h3index.
            hex_lat, hex_lon = h3.h3_to_geo(location_h3index or h3index)
            p = self.image.coord_to_pix(hex_lat, hex_lon)
            self.page_points[page_num].add(p)

            return True

        return False

    def page_num_count(self, page_num: str, marker_cutoff: int) -> int:
        if self.top or len(self.markers) >= marker_cutoff:
            return len(self.page_points.get(page_num, set()))

        return 0

    def photo_markers(self) -> Iterable[Marker]:
        return (marker for markers in self.markers.values() for marker in markers)

    def log(self, debug: Callable[..., None]) -> None:
        debug(f"{self.image.name}")
        for key, markers in self.markers.items():
            grouped_markers = groupby(sorted(markers, key=attrgetter("location")), key=attrgetter("location"))
            for location, location_markers in grouped_markers:
                page_nums = [(marker.page_num, marker.caption) for marker in location_markers]
                photos = self.collapse_page_nums(page_nums)
                debug(f"{key}: {location}: {photos}")

    def get_label_layout(
        self,
        get_marker_caption: Callable[[Marker], str],
        label_width: Fraction,
        label_margin: float,
        scale_x: Fraction,
        marker_radius: Fraction = Fraction(0),
        divider_position: str = "median",
        lhs_order: str = "top",
        rhs_order: str = "top",
        cm_to_px: float = 1.0,
    ) -> tuple[list[Leader], list[Port]]:
        if not self.markers:
            return ([], [])

        # TODO: should mid-point be moved to minimise overlaps?
        # (i.e. split problematic clusters)
        if divider_position == "median":  # noqa: SIM108
            mid_x = median(m.x for m in self.markers)
        else:
            mid_x = self.image.width / 2.0
        # canvas.line(mid_x, 0, mid_x, self.image.height)

        layout = LabelLayout(
            self.image.width,
            self.image.height,
            label_margin / scale_x,
            mid_x,
            lhs_order=lhs_order,
            rhs_order=rhs_order,
            marker_radius=marker_radius,
        )

        def _label(markers: list[Marker], separator: str = ": ") -> Iterable[str]:
            grouped_markers = groupby(
                sorted(markers, key=attrgetter("location", "page_num_key")),
                key=attrgetter("location"),
            )
            for location, location_markers in grouped_markers:
                location_markers2 = list(location_markers)
                page_nums = [(marker.page_num, get_marker_caption(marker)) for marker in location_markers2]
                photos = self.collapse_page_nums(page_nums)
                if location:
                    yield f"{location}{separator}{photos}"
                else:
                    yield photos

        for p, markers in self.markers.items():
            align = Align.Right if p.x < mid_x else Align.Left
            para = Label.make_label2(
                "<br/>".join(_label(markers, ": ")),
                "<br/>".join(_label(markers, "<br/>")),
                align,
                scale_x,
                label_width,
                markers,
                marker_radius,
            )
            layout.add_point(p.x, p.y, para.height * cm_to_px, para)

        def eq(data1: Any, data2: Any) -> bool:
            return {marker.location for marker in data1.markers} == {marker.location for marker in data2.markers}

        def merge_data(data: list[Any]) -> Any:
            if len(data) == 1:
                return data[0]

            markers = [marker for label in data for marker in label.markers]
            return Label.make_label2(
                "<br/>".join(_label(markers, ": ")),
                "<br/>".join(_label(markers, "<br/>")),
                data[0].align,
                scale_x,
                label_width,
                markers,
                marker_radius,
            )

        return layout.get_merged_labels(eq=eq, merge_data=merge_data)

    def collapse_page_nums(self, page_nums: list[PageNum]) -> str:
        def has_multiple_days(_page_num: str) -> bool:
            return False

        return ", ".join(collapse_items(page_nums, continue_range(has_multiple_days), page_range))


class Index:
    def __init__(self, cal: BookCalendar) -> None:
        self.cal = cal
        self.month_days: dict[int, list[PageNum]] = defaultdict(list)
        # list of captions for the day
        self.month_day_captions: dict[int, list[str]] = defaultdict(list)
        self.page_days: dict[str, set[int]] = defaultdict(set)
        self.day_hours: dict[int, list[str]] = defaultdict(list)

    def add_photo(
        self,
        year_month: tuple[int, int] | None,
        day: int,
        hour: str,
        page_num: str,
        caption: str,
        cal_caption: str | None,
    ) -> bool:
        # filter out photos from a different month
        if year_month != (self.cal.year, self.cal.month):
            return False

        if cal_caption is None:
            return False

        self.day_hours[day].append(hour)
        self.page_days[page_num].add(day)
        self.month_days[day].append((page_num, caption))

        self.month_day_captions[day].append(cal_caption)

        # TODO: graphic of times for each photo
        # - has to be a 12 hr clock to be understandable
        # - icon/colour/shade am/pm ◁ ▶

        return True

    def log(self, debug: Callable[..., None]) -> None:
        debug(f"{self.cal.month_name} {self.cal.year}")
        for day, day_name in self.cal.days:
            page_nums = self.month_days[day]
            photos = self.collapse_page_nums(page_nums)
            hours = ", ".join(sorted(self.day_hours[day]))
            debug(f"{day_name[:3]} {day}: {photos} {hours}")

    def index_table_data(self, para: Callable[[str], Any]) -> list[TableDataType]:
        data: list[TableDataType] = []
        data.append([f"{self.cal.month_name} {self.cal.year}", "", ""])
        for day, day_name in self.cal.days:
            # single page entry if all slots are the same day
            page_nums = [
                (page_num, caption if self.has_multiple_days(page_num) else "")
                for page_num, caption in self.month_days[day]
            ]

            photos = self.collapse_page_nums(page_nums)

            cal_captions = self.collapse_cal_captions(self.month_day_captions[day])
            if cal_captions:
                photos += f" &mdash; {cal_captions}"

            data.append([f"{day}", f"{day_name[:3]}", para(photos)])

        return data

    @staticmethod
    def collapse_cal_captions(cal_captions: list[str]) -> str:
        unique_captions = []
        for cal_caption in cal_captions:
            if cal_caption and cal_caption not in unique_captions:
                unique_captions.append(cal_caption)
        return ", ".join(unique_captions)

    @staticmethod
    def index_table(data: list[TableDataType], width: Fraction) -> Table:
        a = Fraction("0.35")
        b = Fraction("0.60")
        table = Table(data, [(a + a), (b + a), (width - b - a - a - a)])

        table.setStyle(
            [
                # whole table
                ("FONTNAME", (0, 0), (-1, -1), "Vera"),
                ("FONTSIZE", (0, 0), (-1, -1), 8),
                ("LEADING", (0, 0), (-1, -1), 11),
                ("VALIGN", (0, 0), (-1, -1), "TOP"),
                ("LEFTPADDING", (0, 0), (-1, -1), 0),
                ("RIGHTPADDING", (0, 0), (-1, -1), 0.5 * cm),
                ("BOTTOMPADDING", (0, 0), (-1, -1), 0),
                ("TOPPADDING", (0, 0), (-1, -1), 0),
                # heading
                ("SPAN", (0, 0), (-1, 0)),
                ("FONTNAME", (0, 0), (-1, 0), "VeraIt"),
                ("FONTSIZE", (0, 0), (-1, 0), 10),
                ("LEADING", (0, 0), (-1, 0), 20),
            ]
        )

        return table

    def collapse_page_nums(self, page_nums: list[PageNum]) -> str:
        return ", ".join(collapse_items(page_nums, continue_range(self.has_multiple_days), page_range))

    def has_multiple_days(self, page_num: str) -> bool:
        return len(self.page_days[page_num]) > 1


def continue_range(
    has_multiple_days: Callable[[str], bool],
) -> Callable[[list[PageNum], PageNum], bool]:
    def inner(stack: list[PageNum], page_num: PageNum) -> bool:
        # nothing to continue
        if not stack:
            return False

        if has_multiple_days(page_num[0]):
            return False

        # page out of sequence
        try:
            this = int(page_num[0])
            prev = int(stack[-1][0])
        except ValueError:
            return False  # don't continue over cover pages
        else:
            if prev != this - 1:
                return False

        return True

    return inner


def page_range(stack: list[PageNum], sep: str = "&ndash;") -> Iterable[str]:
    if len(stack) > 2:  # noqa: PLR2004
        yield f"{_page_num(*stack[0])}{sep}{_page_num(*stack[-1])}"
    else:
        for page_num, caption in stack:
            yield _page_num(page_num, caption)


def _page_num(page_num: str, caption: str) -> str:
    if caption:
        return f"{page_num} {caption}"

    return f"{page_num}"


def fetch_map(
    center_lon: float,
    center_lat: float,
    zoom: float,
    slug: str,
    width: int,
    height: int,
    mapbox_key: str,
    map_path: Path = Path("/uploads/maps"),
) -> str:
    options = hashlib.md5()  # noqa: S324
    options.update(repr((center_lon, center_lat, zoom)).encode())
    suffix = options.hexdigest()
    filename = map_path / f"{slug}-{width}x{height}_{suffix}.png"
    if not filename.is_file():
        url = "https://api.mapbox.com/styles/v1/mapbox"
        style = "light-v10"
        # getting @2x, so halve width and height
        width2 = int(ceil(width / 2))
        height2 = int(ceil(height / 2))
        req = requests.get(
            f"{url}/{style}/static/"
            f"{center_lon},{center_lat},{zoom}/"
            f"{width2}x{height2}@2x"
            f"?access_token={mapbox_key}",
            timeout=60,
        )
        filename.write_bytes(req.content)
    return str(filename)


class IndexBuilder:
    def __init__(self, cals: list[BookCalendar], maps: list[MapImage]) -> None:
        self.marker_cutoff = 5
        self.cal_indexes = [Index(cal) for cal in cals]
        self.map_indexes = [MapIndex(map_image) for map_image in maps]
        self.map_indexes[-1].top = True

    def add_photo(self, page_num: str, photo: Photo, caption: _Caption) -> None:
        self.add_photo1(photo.year_month, photo.day, photo.hour, page_num, caption.abbrev())

        self.add_photo2(
            photo.h3index,
            page_num,
            caption.abbrev(),
            photo.location.caption if photo.location else "",
            photo.exif_lat,
            photo.exif_lon,
            photo.location.h3index2 if photo.location else photo.h3index,
        )

    def add_photo1(
        self,
        year_month: tuple[int, int] | None,
        day: int,
        hour: str,
        page_num: str,
        slot_caption: str,
        cal_caption: str | None = "",
    ) -> None:
        for index in self.cal_indexes:
            if index.add_photo(year_month, day, hour, page_num, slot_caption, cal_caption):
                break

    def add_photo2(
        self,
        h3index: str | None,
        page_num: str,
        slot_caption: str,
        location_caption: str,
        exif_lat: float | None = None,
        exif_lon: float | None = None,
        location_h3index: str | None = None,
    ) -> None:
        if h3index is None:
            # TODO: need to record "unknown" location for a
            # slot so label can be specific about which slot
            # is at the location if not all slots on the page
            # are at the same location.
            return

        for map_index in self.map_indexes:
            if map_index.add_photo(
                exif_lat,
                exif_lon,
                h3index,
                page_num,
                slot_caption,
                location_caption,
                location_h3index,
            ):
                break

    def index_pages(
        self, cal_slots: list[int], map_slots: list[int]
    ) -> Iterable[tuple[dict[int, Any], dict[int, Any]]]:
        return _index_pages(
            self.cal_indexes,
            cal_slots,
            list(reversed(list(self.included_maps()))),
            map_slots,
        )

    def included_maps(self) -> Iterable[MapIndex]:
        for map_index in self.map_indexes:
            if self.include_map(map_index):
                yield map_index

    def include_map(self, map_index: MapIndex) -> bool:
        if len(map_index.markers) < self.marker_cutoff:
            # move markers up to next map
            next_maps = [map2 for map2 in self.map_indexes if map2 > map_index]
            if next_maps:
                for marker in map_index.photo_markers():
                    for next_map in next_maps:
                        if next_map.add_photo(
                            marker.lat,
                            marker.lon,
                            None,
                            marker.page_num,
                            marker.caption,
                            marker.location,
                            # won't pass in the location_h3index for now on the
                            # assumption that the points will all end up in the
                            # same hex on the next map anyway
                            None,
                        ):
                            break
                return False

        return True

    def get_marker_caption(self, marker: Marker) -> str:
        if sum(map_index.page_num_count(marker.page_num, self.marker_cutoff) for map_index in self.map_indexes) == 1:
            # omit slots if page is only at one point
            return ""

        return marker.caption


def _index_pages(
    cals: list[Any], cal_slots: list[int], maps: list[Any], map_slots: list[int]
) -> Iterable[tuple[dict[int, Any], dict[int, Any]]]:
    if set(cal_slots).intersection(map_slots):
        while cals:
            page_cals, cals = cals[: len(cal_slots)], cals[len(cal_slots) :]
            yield dict(zip(cal_slots, page_cals, strict=False)), {}

        while maps:
            page_maps, maps = maps[: len(map_slots)], maps[len(map_slots) :]
            yield {}, dict(zip(map_slots, page_maps, strict=False))

    else:
        while cals or maps:
            page_cals, cals = cals[: len(cal_slots)], cals[len(cal_slots) :]
            page_maps, maps = maps[: len(map_slots)], maps[len(map_slots) :]
            yield (
                dict(zip(cal_slots, page_cals, strict=False)),
                dict(zip(map_slots, page_maps, strict=False)),
            )
