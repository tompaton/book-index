from fractions import Fraction
from functools import partial
from unittest.mock import MagicMock, Mock, call, patch

import pytest
from label_layout import collapse_items
from pdf_canvas import Align

from book_index.gis_util import Coord, Pix, bounding_box
from book_index.maps import (
    BookCalendar,
    Index,
    IndexBuilder,
    Label,
    MapImage,
    MapIndex,
    Marker,
    PageNum,
    _index_pages,
    continue_range,
    fetch_map,
    page_range,
)


def test_bounding_box() -> None:
    # from js-fiddle using mapbox geo-viewport library
    # https://www.mapbox.com/mapbox.js/example/v1.0.0/static-map-from-bounds-with-geo-viewport/
    # geoViewport.bounds([5.7604079999999955, 45.189756500000016], 10, [600, 300]);
    # --> [5.348968505859375, 45.04441870436415, 6.172943115234375, 45.334771196762766]

    # Note: longitude results differ from the 3rd decimal place...
    assert bounding_box(45.189756500000016, 5.7604079999999955, 10, 600, 300) == (
        Coord(45.334771196762766, 5.347595214843751),
        Coord(45.04441870436415, 6.171569824218755),
    )


@pytest.mark.parametrize(
    ("page_nums", "multiple_days", "result"),
    [
        ([], {}, []),
        ([("1", "")], {}, ["1"]),
        ([("1", "top")], {}, ["1 top"]),
        ([("1", ""), ("2", "")], {}, ["1", "2"]),
        ([("1", ""), ("2", ""), ("3", "")], {}, ["1 - 3"]),
        ([("1", "top"), ("2", ""), ("3", "")], {}, ["1 top - 3"]),
        ([("1", ""), ("2", "top"), ("3", "")], {}, ["1 - 3"]),
        ([("1", ""), ("2", ""), ("3", "top")], {}, ["1 - 3 top"]),
        ([("1", ""), ("3", "")], {}, ["1", "3"]),
        ([("1", ""), ("2", ""), ("3", "")], {"1"}, ["1 - 3"]),
        ([("1", ""), ("2", ""), ("3", "")], {"2"}, ["1", "2", "3"]),
        ([("1", ""), ("2", ""), ("3", "")], {"3"}, ["1", "2", "3"]),
        ([("1", ""), ("1", "")], {}, ["1"]),
        ([("1", ""), ("2", ""), ("2", "")], {}, ["1", "2"]),
        ([("1", ""), ("3", ""), ("3", "")], {}, ["1", "3"]),
        ([("1", ""), ("2", ""), ("2", ""), ("3", "")], {}, ["1 - 3"]),
    ],
)
def test_collapse_page_nums(page_nums: list[PageNum], multiple_days: set[str], result: list[str]) -> None:
    def has_multiple_days(page_num: str) -> bool:
        return page_num in multiple_days

    assert (
        list(
            collapse_items(
                page_nums,
                continue_range(has_multiple_days),
                partial(page_range, sep=" - "),
            )
        )
        == result
    )


@pytest.mark.parametrize(
    ("cals", "cal_slots", "maps", "map_slots", "result"),
    [
        ([], [], [], [], []),
        (["A"], [1], [], [], [({1: "A"}, {})]),
        (["A", "B"], [1], [], [], [({1: "A"}, {}), ({1: "B"}, {})]),
        ([], [], ["A"], [2], [({}, {2: "A"})]),
        (["A", "B"], [1, 2], [], [], [({1: "A", 2: "B"}, {})]),
        (
            ["A", "B"],
            [1],
            ["C", "D"],
            [2],
            [({1: "A"}, {2: "C"}), ({1: "B"}, {2: "D"})],
        ),
        (
            ["A", "B"],
            [1, 2],
            ["C", "D"],
            [1, 2],
            [({1: "A", 2: "B"}, {}), ({}, {1: "C", 2: "D"})],
        ),
    ],
)
def test_index_pages(
    cals: list[str],
    cal_slots: list[int],
    maps: list[str],
    map_slots: list[int],
    result: list[tuple[dict[int, str], dict[int, str]]],
) -> None:
    assert list(_index_pages(cals, cal_slots, maps, map_slots)) == result


@pytest.fixture
def mimage() -> MapImage:
    return MapImage("one", "path/one.png", Coord(1, 2), Coord(3, 4), 100, 200)


def test_mapimage_dlat_dlon(mimage: MapImage) -> None:
    assert (mimage.dlat, mimage.dlon) == (0.01, 0.02)


def test_mapimage_dx_dy(mimage: MapImage) -> None:
    assert (mimage.dx, mimage.dy) == (0.5, 0.5)


@pytest.mark.parametrize(
    ("x", "y", "lat", "lon"),
    [
        (0, 0, 1, 2),
        (100, 200, 3, 4),
        (50, 50, 1.5, 3),
        (-50, -50, 0.5, 1),
        (500, 500, 6, 12),
    ],
)
def test_mapimage_pix_to_coord(mimage: MapImage, x: int, y: int, lat: float, lon: float) -> None:
    assert mimage.pix_to_coord(x, y) == Coord(lat, lon)


@pytest.mark.parametrize(
    ("x", "y", "lat", "lon"),
    [
        (0, 0, 1, 2),
        (1.0, 1.0, 3, 4),
        (0.5, 0.25, 1.5, 3),
        (-0.5, -0.25, 0.5, 1),
        (5.0, 2.5, 6, 12),
    ],
)
def test_mapimage_coord_to_frac(mimage: MapImage, x: float, y: float, lat: float, lon: float) -> None:
    assert mimage.coord_to_frac(lat, lon) == (x, y)


@pytest.mark.parametrize(
    ("x", "y", "lat", "lon"),
    [
        (0, 0, 1, 2),
        (100, 200, 3, 4),
        (50, 50, 1.5, 3),
        (-50, -50, 0.5, 1),
        (500, 500, 6, 12),
    ],
)
def test_mapimage_coord_to_pix(mimage: MapImage, x: int, y: int, lat: float, lon: float) -> None:
    assert mimage.coord_to_pix(lat, lon) == Pix(x, y)


@pytest.mark.parametrize(
    ("lat", "lon", "on_map"),
    [
        (1, 2, True),
        (2.999, 3.999, True),
        (3, 4, False),
        (1.5, 3, True),
        (0.5, 1, False),
        (6, 12, False),
    ],
)
def test_mapimage_coord_on_map(mimage: MapImage, lat: float, lon: float, on_map: bool) -> None:
    assert mimage.coord_on_map(lat, lon) == on_map


@pytest.mark.parametrize(
    ("x", "y", "on_map"),
    [
        (0, 0, True),
        (99, 199, True),
        (100, 200, False),
        (50, 100, True),
        (-50, -50, False),
        (200, 300, False),
    ],
)
def test_mapimage_pix_on_map(mimage: MapImage, x: int, y: int, on_map: bool) -> None:
    assert mimage.pix_on_map(x, y) == on_map


def test_mapimage_h3index_to_pix() -> None:
    a = MapImage("one", "path/one.png", Coord(-37.7, 144.9), Coord(-37.8, 145.0), 100, 200, 10)
    assert a.h3index_to_pix("8fbe63540baca28") == Pix(52, 167)


def test_mapimage_h3_points_inside() -> None:
    a = MapImage("one", "path/one.png", Coord(-37.7, 144.9), Coord(-37.8, 145.0), 100, 200, 10)
    assert a.h3_points("8fbe63540baca28") == [
        (0.5223229430112338, 0.8372927227808923),
        (0.5223122695636858, 0.8372359792683902),
        (0.5222452795439677, 0.8372146215191855),
        (0.5221889629663975, 0.8372500072794985),
        (0.5221996364048506, 0.8373067507918585),
        (0.5222666264299688, 0.8373281085439764),
    ]


def test_mapimage_h3_points_outside() -> None:
    a = MapImage("one", "path/one.png", Coord(-36.7, 144.9), Coord(-36.8, 145.0), 100, 200, 10)
    assert a.h3_points("8fbe63540baca28") is None


def test_mapimage_gt() -> None:
    a = MapImage("one", "path/one.png", Coord(1, 2), Coord(3, 4), 100, 200, 10)
    b = MapImage("one", "path/one.png", Coord(1, 2), Coord(3, 4), 100, 200, 11)

    assert a > b
    assert not b > a


@patch("book_index.maps.Paragraph")
def test_make_label(mock_paragraph: Mock) -> None:
    mock_paragraph.return_value.wrap.return_value = (5, 6)

    lbl = Label.make_label("label", Align.Left, Fraction(1), Fraction(3), [])
    assert lbl == Label(
        mock_paragraph.return_value,
        "label",
        Align.Left,
        Fraction(5),
        Fraction(6),
        Fraction("0.1"),
        [],
    )
    assert mock_paragraph.mock_calls == [
        call(
            "label",
            alignment=Align.Left,
            fontName="Vera",
            fontSize=8.0,
            leading=10.0,
            textColor="black",
        ),
        call().wrap(Fraction(3), Fraction("0.5")),
    ]


@patch("book_index.maps.Paragraph")
def test_make_label_scale(mock_paragraph: Mock) -> None:
    mock_paragraph.return_value.wrap.return_value = (5, 6)

    lbl = Label.make_label("label", Align.Left, Fraction("0.5"), Fraction(3), [])
    assert lbl == Label(
        mock_paragraph.return_value,
        "label",
        Align.Left,
        Fraction(5),
        Fraction(6),
        Fraction("0.2"),
        [],
    )
    assert mock_paragraph.mock_calls == [
        call(
            "label",
            alignment=Align.Left,
            fontName="Vera",
            fontSize=16.0,
            leading=20.0,
            textColor="black",
        ),
        call().wrap(6.0, 1.0),
    ]


@pytest.mark.parametrize(
    ("page_num", "page_num_key"),
    [
        ("1", 1),
        ("cover", -1),
        ("inside front", 0),
        ("inside back", 1000),
        ("back", 1001),
        ("other", 0),
    ],
)
def test_page_num_key(page_num: str, page_num_key: int) -> None:
    assert Marker(page_num, "caption", 1, 2, "location", "h3", 3, 4).page_num_key == page_num_key


def test_mapindex_add_photo_not_on_map() -> None:
    img = Mock(h3resolution=10)
    img.coord_to_pix.return_value = Pix(100, 200)
    img.pix_on_map.return_value = False

    idx = MapIndex(img)
    assert not idx.add_photo(34, 123, None, "1", "slot", "loc", None)
    assert len(idx) == 0

    assert img.mock_calls == [
        call.coord_to_pix(33.99975942469712, 122.99933385705897),
        call.pix_on_map(100, 200),
    ]


def test_mapindex_add_photo_from_h3index() -> None:
    img = Mock(h3resolution=10)
    img.coord_to_pix.return_value = Pix(100, 200)
    img.pix_on_map.return_value = False

    idx = MapIndex(img)
    assert not idx.add_photo(30, 100, "8a308d73254ffff", "1", "slot", "loc", "8a308d73254ffff")
    assert len(idx) == 0

    assert img.mock_calls == [
        call.coord_to_pix(33.99975942469712, 122.99933385705897),
        call.pix_on_map(100, 200),
    ]


def test_mapindex_add_photo() -> None:
    img = Mock(h3resolution=10)
    img.coord_to_pix.return_value = Pix(100, 200)
    img.pix_on_map.return_value = True

    idx = MapIndex(img)
    assert idx.add_photo(34, 123, None, "1", "slot", "loc", None)
    assert len(idx) == 1

    assert img.mock_calls == [
        call.coord_to_pix(33.99975942469712, 122.99933385705897),
        call.pix_on_map(100, 200),
        call.coord_to_pix(33.99975942469712, 122.99933385705897),
    ]

    assert idx.markers == {
        Pix(100, 200): [
            Marker(
                page_num="1",
                caption="slot",
                lat=34,
                lon=123,
                location="loc",
                h3index="8a308d73254ffff",
                x=100,
                y=200,
            )
        ]
    }

    assert list(idx.photo_markers()) == [
        Marker(
            page_num="1",
            caption="slot",
            lat=34,
            lon=123,
            location="loc",
            h3index="8a308d73254ffff",
            x=100,
            y=200,
        )
    ]

    assert idx.page_points == {"1": {Pix(100, 200)}}


def test_mapindex_log() -> None:
    img = Mock(h3resolution=10)
    img.name = "Map name"
    img.pix_on_map.return_value = True

    idx = MapIndex(img)
    img.coord_to_pix.return_value = Pix(100, 200)
    assert idx.add_photo(34, 123, None, "1", "Top", "locA", None)
    assert idx.add_photo(34.1, 123, None, "2", "Bottom", "locA", None)
    assert idx.add_photo(34, 123.1, None, "1", "Left", "locB", None)
    img.coord_to_pix.return_value = Pix(150, 250)
    assert idx.add_photo(34, 123, None, "3", "Top", "locA", None)
    assert len(idx) == 4

    debug = Mock()
    idx.log(debug)

    assert debug.mock_calls == [
        call("Map name"),
        call("(100, 200): locA: 1 Top, 2 Bottom"),
        call("(100, 200): locB: 1 Left"),
        call("(150, 250): locA: 3 Top"),
    ]


def test_mapindex_page_num_count() -> None:
    img = Mock(h3resolution=10)
    img.coord_to_pix.return_value = Pix(100, 200)
    img.pix_on_map.return_value = True

    idx = MapIndex(img)
    assert idx.add_photo(34, 123, None, "1", "slot", "loc", None)
    assert len(idx) == 1

    assert idx.page_num_count("2", 5) == 0
    assert idx.page_num_count("1", 5) == 0
    assert idx.page_num_count("2", 0) == 0
    assert idx.page_num_count("1", 0) == 1

    idx.top = True
    assert idx.page_num_count("2", 5) == 0
    assert idx.page_num_count("1", 5) == 1


def test_index_add_photo_wrong_month() -> None:
    cal = BookCalendar(2010, 11, "Nov", [])
    idx = Index(cal)
    assert not idx.add_photo((2010, 12), 12, "13:00", "1", "Top", "Xyz")


def test_index_add_photo() -> None:
    cal = BookCalendar(2010, 11, "Nov", [])
    idx = Index(cal)
    assert idx.add_photo((2010, 11), 12, "13:00", "1", "Top", "Xyz")
    assert idx.month_days == {12: [("1", "Top")]}
    assert idx.page_days == {"1": {12}}
    assert idx.day_hours == {12: ["13:00"]}


def test_index_log() -> None:
    cal = BookCalendar(2010, 11, "Nov", [(11, "Monday"), (12, "Tuesday"), (13, "Wednesday")])
    idx = Index(cal)
    idx.add_photo((2010, 11), 12, "13:00", "1", "Top", "Xyz")
    debug = Mock()
    idx.log(debug)
    assert debug.mock_calls == [
        call("Nov 2010"),
        call("Mon 11:  "),
        call("Tue 12: 1 Top 13:00"),
        call("Wed 13:  "),
    ]


def test_index_index_table_data() -> None:
    cal = BookCalendar(2010, 11, "November", [(11, "Monday"), (12, "Tuesday"), (13, "Wednesday")])
    idx = Index(cal)
    idx.add_photo((2010, 11), 12, "13:00", "1", "Top", "Xyz")

    mock_paragraph = Mock()
    assert idx.index_table_data(mock_paragraph) == [
        ["November 2010", "", ""],
        ["11", "Mon", mock_paragraph.return_value],
        ["12", "Tue", mock_paragraph.return_value],
        ["13", "Wed", mock_paragraph.return_value],
    ]

    assert mock_paragraph.mock_calls == [call(""), call("1 &mdash; Xyz"), call("")]


@patch("book_index.maps.Table")
def test_index_index_table(mock_table: Mock) -> None:
    result = Index.index_table([], Fraction(50))
    assert mock_table.mock_calls == [
        call([], [Fraction("0.7"), Fraction("0.95"), Fraction("48.35")]),
        call().setStyle(
            [
                ("FONTNAME", (0, 0), (-1, -1), "Vera"),
                ("FONTSIZE", (0, 0), (-1, -1), 8),
                ("LEADING", (0, 0), (-1, -1), 11),
                ("VALIGN", (0, 0), (-1, -1), "TOP"),
                ("LEFTPADDING", (0, 0), (-1, -1), 0),
                ("RIGHTPADDING", (0, 0), (-1, -1), pytest.approx(14.173228)),
                ("BOTTOMPADDING", (0, 0), (-1, -1), 0),
                ("TOPPADDING", (0, 0), (-1, -1), 0),
                ("SPAN", (0, 0), (-1, 0)),
                ("FONTNAME", (0, 0), (-1, 0), "VeraIt"),
                ("FONTSIZE", (0, 0), (-1, 0), 10),
                ("LEADING", (0, 0), (-1, 0), 20),
            ]
        ),
    ]
    assert result == mock_table.return_value


def test_fetch_map_cached() -> None:
    mock_path = MagicMock()
    mock_path.__truediv__.return_value.is_file.return_value = True
    filename = "/uploads/maps/slug-4x5_41ffb5fa670d6b16c2786e81d4fafa7c.png"
    mock_path.__truediv__.return_value.__str__.return_value = filename

    assert fetch_map(1, 2, 3, "slug", 4, 5, "apikey", mock_path) == filename
    mock_path.__truediv__.assert_called_with("slug-4x5_41ffb5fa670d6b16c2786e81d4fafa7c.png")


@patch("requests.get")
def test_fetch_map(mock_requests: Mock) -> None:
    mock_path = MagicMock()
    mock_path.__truediv__.return_value.is_file.return_value = False
    filename = "/uploads/maps/slug-4x5_fd733f9b81a4c4336dcce3886c5ed3d3.png"
    mock_path.__truediv__.return_value.__str__.return_value = filename

    assert fetch_map(10, 20, 3, "slug", 4, 5, "apikey", mock_path) == filename

    mock_path.__truediv__.assert_called_with("slug-4x5_fd733f9b81a4c4336dcce3886c5ed3d3.png")

    mock_path.__truediv__.return_value.write_bytes.assert_called_with(mock_requests.return_value.content)

    assert mock_requests.mock_calls == [
        call(
            "https://api.mapbox.com/styles/v1/mapbox" "/light-v10/static/10,20,3/2x3@2x?access_token=apikey",
            timeout=60,
        )
    ]


@patch("book_index.maps.Index")
@patch("book_index.maps.MapIndex")
def test_indexbuilder(mock_mapindex: Mock, mock_index: Mock) -> None:
    cal = Mock()
    map_image = Mock()
    IndexBuilder([cal], [map_image])
    assert mock_mapindex.mock_calls == [call(map_image)]
    assert mock_index.mock_calls == [call(cal)]
    assert mock_mapindex.return_value.top is True


@patch("book_index.maps.Index")
@patch("book_index.maps.MapIndex")
def test_indexbuilder_add_photo_no_h3index(mock_mapindex: Mock, mock_index: Mock) -> None:
    cal = Mock()
    mock_index.return_value.add_photo.return_value = True
    map_image = Mock()
    builder = IndexBuilder([cal], [map_image])
    photo = Mock(year_month=(2010, 11), day=12, hour="13:00", h3index=None)
    caption = Mock()
    builder.add_photo("1", photo, caption)
    assert mock_index.mock_calls == [
        call(cal),
        call().add_photo((2010, 11), 12, "13:00", "1", caption.abbrev.return_value, ""),
    ]
    assert mock_mapindex.mock_calls == [call(map_image)]


@patch("book_index.maps.Index")
@patch("book_index.maps.MapIndex")
def test_indexbuilder_add_photo(mock_mapindex: Mock, mock_index: Mock) -> None:
    cal = Mock()
    mock_index.return_value.add_photo.return_value = False
    map_image = Mock()
    mock_mapindex.return_value.add_photo.return_value = True
    builder = IndexBuilder([cal], [map_image])
    photo = Mock(
        year_month=(2010, 11),
        day=12,
        hour="13:00",
        h3index="0001000200030004",
        exif_lat=34,
        exif_lon=123,
        location=None,
    )
    caption = Mock()
    builder.add_photo("1", photo, caption)
    assert mock_index.mock_calls == [
        call(cal),
        call().add_photo((2010, 11), 12, "13:00", "1", caption.abbrev.return_value, ""),
    ]
    assert mock_mapindex.mock_calls == [
        call(map_image),
        call().add_photo(
            34,
            123,
            "0001000200030004",
            "1",
            caption.abbrev.return_value,
            "",
            "0001000200030004",
        ),
    ]


@patch("book_index.maps.Index")
@patch("book_index.maps.MapIndex")
def test_indexbuilder_add_photo2(mock_mapindex: Mock, mock_index: Mock) -> None:
    cal = Mock()
    mock_index.return_value.add_photo.return_value = False
    map_image = Mock()
    mock_mapindex.return_value.add_photo.return_value = False
    builder = IndexBuilder([cal], [map_image])
    photo = Mock(
        year_month=(2010, 11),
        day=12,
        hour="13:00",
        h3index="0001000200030004",
        exif_lat=34,
        exif_lon=123,
        location=Mock(caption="location", h3index2="0002001004003"),
    )
    caption = Mock()
    builder.add_photo("1", photo, caption)
    assert mock_index.mock_calls == [
        call(cal),
        call().add_photo((2010, 11), 12, "13:00", "1", caption.abbrev.return_value, ""),
    ]
    assert mock_mapindex.mock_calls == [
        call(map_image),
        call().add_photo(
            34,
            123,
            "0001000200030004",
            "1",
            caption.abbrev.return_value,
            "location",
            "0002001004003",
        ),
    ]
