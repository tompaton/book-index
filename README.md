# Book Index

Index builder for photo book.


[![PyPI - Version](https://img.shields.io/pypi/v/book-index.svg)](https://pypi.org/project/book-index)
[![PyPI - Python Version](https://img.shields.io/pypi/pyversions/book-index.svg)](https://pypi.org/project/book-index)

-----

## Table of Contents

- [Installation](#installation)
- [License](#license)

## Installation

```console
pip install book-index
```

## License

`book-index` is distributed under the terms of the [MIT](https://spdx.org/licenses/MIT.html) license.
